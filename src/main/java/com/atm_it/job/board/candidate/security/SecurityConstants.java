package com.atm_it.job.board.candidate.security;

/**
 * Created by ATAF MOHELLEBI on 17/11/2020.
 */
public class SecurityConstants {

    public static final String SECRET          = "SecretKeyToGenJWTs";
    public static final long   EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX    = "Bearer ";
    public static final String HEADER_STRING   = "Authorization";
    public static final String SIGN_UP_URL     = "/users/sign-up";

    private SecurityConstants() {

    }
}
