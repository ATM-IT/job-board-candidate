package com.atm_it.job.board.candidate.security;

import java.util.Collections;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;
import lombok.SneakyThrows;

/**
 * Created by ATAF MOHELLEBI on 17/11/2020.
 */
@Service
public class UserService implements UserDetailsService {

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        UserRecord user = firebaseAuth.getUserByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(email);
        }
        return new User(user.getEmail(), user.getUid(), Collections.emptyList());
    }
}
