package com.atm_it.job.board.candidate.security;

import lombok.Builder;
import lombok.Data;

/**
 * Created by ATAF MOHELLEBI on 08/11/2020.
 */
@Data
public class AccountCredentials {

    private String email;
    private String password;
}

@Data
@Builder
class TokenResponse {
    private String token;
}