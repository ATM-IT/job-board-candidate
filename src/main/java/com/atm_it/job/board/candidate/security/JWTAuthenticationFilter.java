package com.atm_it.job.board.candidate.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;

import static com.atm_it.job.board.candidate.security.SecurityConstants.EXPIRATION_TIME;
import static com.atm_it.job.board.candidate.security.SecurityConstants.HEADER_STRING;
import static com.atm_it.job.board.candidate.security.SecurityConstants.SECRET;
import static com.atm_it.job.board.candidate.security.SecurityConstants.TOKEN_PREFIX;
import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

/**
 * Created by ATAF MOHELLEBI on 17/11/2020.
 */
@Slf4j
@Order(1)
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(final AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {

        try {
            AccountCredentials user = new ObjectMapper()
                                        .readValue(req.getInputStream(), AccountCredentials.class);
            return authenticationManager.authenticate(
              new UsernamePasswordAuthenticationToken(
                user.getEmail(),
                user.getPassword(),
                new ArrayList<>()));
        } catch (IOException e) {
            log.error("Error during Authentication {}", e.getMessage());
            throw new RuntimeException(e);
        }

    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException {

        Gson gson = new Gson();
        String token = JWT.create()
                          .withSubject(((User) auth.getPrincipal()).getUsername())
                          .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                          .sign(HMAC512(SECRET.getBytes()));
        TokenResponse tokenResponse = TokenResponse.builder()
                                                   .token(token)
                                                   .build();
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
        res.getWriter().write(gson.toJson(tokenResponse));
        res.getWriter().flush();
        res.getWriter().close();
    }

}
